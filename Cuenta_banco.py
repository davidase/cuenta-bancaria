class Cuenta:
    def __init__(self, nombre, numero, saldo=0):
        self.titular = nombre
        self.numero = numero
        self.saldo = saldo
        pass

    def ingreso(self, cantidad):
        self.saldo = self.saldo + cantidad


    def reintegro(self, cantidad):
        self.saldo = self.saldo - cantidad

    def transferenciaM(self, dst, cantidad): #Es una orden al objeto.
        self.reintegro(cantidad)
        dst.ingreso(cantidad)

    def transferenciaF(org, dst, cantidad): #La función no tiene el self porque no utiliza los atributos de la clase.
        org.reintegro(cantidad)
        dst.ingreso(cantidad)

    def __str__(self):
        return "{} con saldo: {}". format(self.numero, self.saldo)

cliente1 = Cuenta('Pepe', '1234567')
cliente2 = Cuenta('Ana', '7654321', 5000)

print(cliente1)
print(cliente2)

cliente1.ingreso(3000)
print(cliente1)

cliente1.reintegro(10)
print(cliente1)

cliente1.transferenciaM(cliente2, 30)
print(cliente1)

Cuenta.transferenciaF(cliente2, cliente1, 30)
print(cliente1)